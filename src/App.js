import cssModule from'./App.module.css'
import image from "./assets/images/48.jpg";
function App() {
  return (
    <div>
      <div className={cssModule.dcContainer}>

        <div className={cssModule.dcImageContainer}>
          <img  className={cssModule.dcImage} src={image} alt='image user'></img>
        </div>

        <div className={cssModule.dcQuote}>
          <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills </p>
        </div>

        <div className={cssModule.dcInfo}>
          <b> Tammy Srevens </b> * Front End Developer
        </div>

      </div>
    </div>
  );
}

export default App;
